package ru.myhelpit.quotes.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.myhelpit.quotes.utils.QuoteCache;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class QuoteControllerTest {
    @Autowired
    private MockMvc mvc;

    @BeforeEach
    void setUp() {
        QuoteCache.getIsinLevelMap().clear();
    }

    @Test
    void addQuote_less_isin_lenght() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/quote/add")
                .content("{\"isin\": \"RU000A0JX0J\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("data").isEmpty())
                .andExpect(jsonPath("errors").value("isin must be 12 characters"));
    }

    @Test
    void addQuote_ask_lower_bid() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/quote/add")
                .content("{\"isin\": \"RU000A0JX0J2\", \"bid\": \"101.0\", \"ask\": \"100.0\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("data").isEmpty())
                .andExpect(jsonPath("errors").value("bid must be lower than ask"));
    }

    @Test
    void addQuote() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/quote/add")
                .content("{\"isin\": \"RU000A0JX0J2\", \"bid\": \"100.0\", \"ask\": \"100.1\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void getIsins() throws Exception {
        String isinName = "RU000A0JX0J2";
        mvc.perform(MockMvcRequestBuilders.post("/quote/add")
                .content("{\"isin\": \"RU000A0JX0J2\", \"bid\": \"100.0\", \"ask\": \"100.1\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(MockMvcRequestBuilders.get("/quote/isins"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("data").value(isinName));
    }

    @Test
    void getEnergyLevel() throws Exception {
        String isinName = "RU000A0JX0J2";
        mvc.perform(MockMvcRequestBuilders.post("/quote/add")
                .content("{\"isin\": \"RU000A0JX0J2\", \"bid\": \"100.2\", \"ask\": \"101.9\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(MockMvcRequestBuilders.get("/quote/level?isin="+isinName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("data").value(100.2));

        mvc.perform(MockMvcRequestBuilders.post("/quote/add")
                .content("{\"isin\": \"RU000A0JX0J2\", \"bid\": \"100.5\", \"ask\": \"101.9\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mvc.perform(MockMvcRequestBuilders.get("/quote/level?isin="+isinName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("data").value(100.5));
    }
}