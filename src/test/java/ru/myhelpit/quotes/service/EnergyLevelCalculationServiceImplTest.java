package ru.myhelpit.quotes.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.myhelpit.quotes.dto.QuoteDto;
import ru.myhelpit.quotes.utils.QuoteCache;

import static org.junit.Assert.assertEquals;

class EnergyLevelCalculationServiceImplTest {
    private static final String ISIN_NAME = "RU000A0JX0J2";
    private static final Double ASK = 102.0;
    private static final Double ASK_2 = 101.0;
    private static final Double BID = 100.0;
    private static final Double BID_2 = 100.5;

    @BeforeEach
    public void setUp(){
        QuoteCache.getIsinLevelMap().clear();
    }

    @Test
    void updateEnergyLevel_new_quote_without_bid() {
        EnergyLevelCalculationServiceImpl service = new EnergyLevelCalculationServiceImpl();
        service.updateEnergyLevel(getQuoteDto(null, ASK));
        assertEquals(ASK, QuoteCache.getIsinLevelMap().get(ISIN_NAME));
    }

    @Test
    void updateEnergyLevel_new_quote() {
        EnergyLevelCalculationServiceImpl service = new EnergyLevelCalculationServiceImpl();
        service.updateEnergyLevel(getQuoteDto(BID, ASK));
        assertEquals(BID, QuoteCache.getIsinLevelMap().get(ISIN_NAME));
    }

    @Test
    void updateEnergyLevel_energy_level_less_bid() {
        EnergyLevelCalculationServiceImpl service = new EnergyLevelCalculationServiceImpl();
        service.updateEnergyLevel(getQuoteDto(BID, ASK));
        service.updateEnergyLevel(getQuoteDto(BID_2, ASK));
        assertEquals(BID_2, QuoteCache.getIsinLevelMap().get(ISIN_NAME));
    }

    @Test
    void updateEnergyLevel_ask_less_energy_level() {
        EnergyLevelCalculationServiceImpl service = new EnergyLevelCalculationServiceImpl();
        service.updateEnergyLevel(getQuoteDto(null, ASK));
        service.updateEnergyLevel(getQuoteDto(BID, ASK_2));
        assertEquals(ASK_2, QuoteCache.getIsinLevelMap().get(ISIN_NAME));
    }

    @Test
    void updateEnergyLevel_no_change_energy_level() {
        EnergyLevelCalculationServiceImpl service = new EnergyLevelCalculationServiceImpl();
        service.updateEnergyLevel(getQuoteDto(null, ASK_2));
        assertEquals(ASK_2, QuoteCache.getIsinLevelMap().get(ISIN_NAME));

        service.updateEnergyLevel(getQuoteDto(BID, ASK));
        assertEquals(ASK_2, QuoteCache.getIsinLevelMap().get(ISIN_NAME));
    }

    private QuoteDto getQuoteDto(Double bid, Double ask){
        QuoteDto quoteDto = new QuoteDto();
        quoteDto.setIsin(ISIN_NAME);
        quoteDto.setBid(bid);
        quoteDto.setAsk(ask);
        return quoteDto;
    }
}