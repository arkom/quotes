package ru.myhelpit.quotes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.myhelpit.quotes.dto.QuoteDto;
import ru.myhelpit.quotes.repository.QuoteRepository;
import ru.myhelpit.quotes.utils.QuoteCache;

import java.util.Collections;
import java.util.List;

@Service
public class QuoteServiceImpl implements QuoteService {
    private EnergyLevelCalculationService calculationService;
    private QuoteRepository quoteRepository;

    @Autowired
    public QuoteServiceImpl(EnergyLevelCalculationService calculationService, QuoteRepository quoteRepository) {
        this.calculationService = calculationService;
        this.quoteRepository = quoteRepository;
    }

    @Override
    public void addQuote(QuoteDto quoteDto) {
        quoteRepository.save(quoteDto.toQuote());
        calculationService.updateEnergyLevel(quoteDto);
    }

    @Override
    public List<String> getIsins() {
        return Collections.list(QuoteCache.getIsinLevelMap().keys());
    }

    @Override
    public Double getLevelByIsin(String isin) {
        return QuoteCache.getIsinLevelMap().get(isin);
    }
}
