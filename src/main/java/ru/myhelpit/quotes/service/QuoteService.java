package ru.myhelpit.quotes.service;

import ru.myhelpit.quotes.dto.QuoteDto;

import java.util.List;

public interface QuoteService {
    void addQuote(QuoteDto quoteDto);
    List<String> getIsins();
    Double getLevelByIsin(String isin);
}
