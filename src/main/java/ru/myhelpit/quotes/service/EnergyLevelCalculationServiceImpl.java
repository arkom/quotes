package ru.myhelpit.quotes.service;

import org.springframework.stereotype.Service;
import ru.myhelpit.quotes.utils.Locks;
import ru.myhelpit.quotes.utils.QuoteCache;
import ru.myhelpit.quotes.dto.QuoteDto;

import java.util.concurrent.locks.Lock;

@Service
public class EnergyLevelCalculationServiceImpl implements EnergyLevelCalculationService {
    @Override
    public void updateEnergyLevel(QuoteDto quoteDto) {
        Lock lock = Locks.getLocks().get(quoteDto.getIsin());
        lock.lock();
        try {
            Double levelValue = calculateLevelValue(quoteDto);
            QuoteCache.getIsinLevelMap().put(quoteDto.getIsin(), levelValue);
        } finally {
            lock.unlock();
        }
    }

    private Double calculateLevelValue(QuoteDto quoteDto) {
        Double currentLevel = QuoteCache.getIsinLevelMap().get(quoteDto.getIsin());

        if (currentLevel == null) {
            return quoteDto.getBid() != null ? quoteDto.getBid() : quoteDto.getAsk(); //todo what if ask is null?
        }
        if (quoteDto.getBid() != null && quoteDto.getBid() > currentLevel) {
            return quoteDto.getBid();
        }
        if (quoteDto.getAsk() != null && quoteDto.getAsk() < currentLevel) {
            return quoteDto.getAsk();
        }
        // bid <= currentLevel <= ask
        return currentLevel;
    }
}
