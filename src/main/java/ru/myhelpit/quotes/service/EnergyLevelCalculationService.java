package ru.myhelpit.quotes.service;

import ru.myhelpit.quotes.dto.QuoteDto;

public interface EnergyLevelCalculationService {
    void updateEnergyLevel(QuoteDto quoteDto);
}
