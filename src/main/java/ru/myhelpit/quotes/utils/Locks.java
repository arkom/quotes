package ru.myhelpit.quotes.utils;

import com.google.common.util.concurrent.Striped;
import lombok.Getter;

import java.util.concurrent.locks.Lock;

public class Locks {
    @Getter
    private final static Striped<Lock> locks = Striped.lazyWeakLock(100);
}
