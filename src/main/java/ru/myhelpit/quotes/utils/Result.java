package ru.myhelpit.quotes.utils;

import java.util.ArrayList;
import java.util.List;

public class Result {
    private List<String> errors = new ArrayList<>();

    public boolean isValid(){
        return errors.isEmpty();
    }

    public void addError(String err){
        errors.add(err);
    }

    public List<String> getErrors(){
        return new ArrayList<>(errors);
    }
}
