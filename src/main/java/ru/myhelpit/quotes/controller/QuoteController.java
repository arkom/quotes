package ru.myhelpit.quotes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.myhelpit.quotes.dto.QuoteDto;
import ru.myhelpit.quotes.dto.ResponseDto;
import ru.myhelpit.quotes.service.QuoteService;
import ru.myhelpit.quotes.utils.Result;

import java.util.Collections;

@RestController
@RequestMapping("/quote")
public class QuoteController {
    private QuoteService quoteService;

    @Autowired
    public QuoteController(QuoteService quoteService) {
        this.quoteService = quoteService;
    }

    @PostMapping(value = "/add")
    public ResponseEntity addQuote(@RequestBody QuoteDto quoteDto) {
        Result result = validate(quoteDto);
        if (!result.isValid()) {
            ResponseDto responseDto = new ResponseDto(Collections.EMPTY_LIST, result.getErrors());
            return new ResponseEntity(responseDto, HttpStatus.BAD_REQUEST);
        }
        quoteService.addQuote(quoteDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/isins")
    public ResponseEntity getIsins() {
        return ResponseEntity.ok(new ResponseDto(quoteService.getIsins()));
    }

    @GetMapping("/level")
    @ResponseBody
    public ResponseEntity getEnergyLevel(@RequestParam String isin) {
        if (isin == null && isin.length() != 12){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(new ResponseDto(Collections.singletonList(quoteService.getLevelByIsin(isin))));
    }

    private Result validate(QuoteDto quote) {
        Result result = new Result();
        if (quote.getIsin() == null || quote.getIsin().length() != 12) {
            result.addError("isin must be 12 characters");
        }
        if (quote.getAsk() != null && quote.getBid() != null && quote.getAsk() <= quote.getBid()) {
            result.addError("bid must be lower than ask");
        }
        return result;
    }
}
