package ru.myhelpit.quotes.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.myhelpit.quotes.model.Quote;

@Repository
public interface QuoteRepository extends CrudRepository<Quote, Long> {
}
