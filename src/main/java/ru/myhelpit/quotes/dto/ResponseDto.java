package ru.myhelpit.quotes.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;

@Getter
@Setter
public class ResponseDto {
    private Collection data = new ArrayList<>();
    private Collection errors = new ArrayList<>();

    public ResponseDto(Collection data) {
        this.data = data;
    }

    public ResponseDto(Collection data, Collection errors) {
        this.data = data;
        this.errors = errors;
    }
}
