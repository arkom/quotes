package ru.myhelpit.quotes.dto;

import lombok.Getter;
import lombok.Setter;
import ru.myhelpit.quotes.model.Quote;

import java.time.LocalDateTime;

@Getter
@Setter
public class QuoteDto {
    private String isin;
    private Double bid;
    private Double ask;

    public Quote toQuote(){
        Quote quote = new Quote();
        quote.setIsin(isin);
        quote.setAsk(ask);
        quote.setBid(bid);
        quote.setInsertStamp(LocalDateTime.now());
        return quote;
    }
}
