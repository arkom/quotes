package ru.myhelpit.quotes.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "quote")
@Getter
@Setter
public class Quote {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "isin", nullable = false, length = 12)
    private String isin;
    private Double bid;
    private Double ask;
    @Column(name = "insstmp", nullable = false)
    private LocalDateTime insertStamp;
}
